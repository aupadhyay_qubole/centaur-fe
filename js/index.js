var db = TAFFY();
var table;

$(document).ready(function () {
    setTimeout(function () {
        $('body').addClass('loaded');
    }, 1000);
    $('#versionTag').text(VERSION);
    getRegressionList();
    updateWidgetsStats();
    buildTabHeaders();
    buildOverview();
});

function getRegressionList() {
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/regressionlist/",
        type: 'GET',
        async: false,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            buildRegressionListTable(res);
        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function buildTabHeaders() {
    var tab = document.getElementById("regressionTypeTab");
    var regressionTypes = db().distinct("regression_job");
    console.log(regressionTypes);
    var firstTab;
    for (i = 0; i < regressionTypes.length; i++) {
        var li = document.createElement('LI');

        li.className = 'nav-item';
        var a = document.createElement('A');
        var text = document.createTextNode(regressionTypes[i].toString().replace(/_/g, ' '));
        if (i === 0)
            a.className = 'nav-link active';
        else
            a.className = 'nav-link';
        a.role = 'tab';
        a.dataset.toggle = 'tab';
        a.name = regressionTypes[i];
        a.href = 'javascript:filterTabs(this.name);';
        a.onclick = function () { filterTabs(0, this.name) };
        a.appendChild(text);
        li.appendChild(a);
        tab.appendChild(li);

        if (i === 0)
            firstTab = regressionTypes[i];
    }

    filterTabs(0, firstTab);
}

function filterTabs(column, text) {
    table.columns(column).search(text.toString().replace(/_/g, ' ')).draw();
}

function buildOverview() {

    var regressionTypes = db().distinct("regression_job");
    var overviewGroup = document.getElementById('overview');
    for (i = 0; i < regressionTypes.length; i++) {
        var cardDiv = document.createElement('DIV');
        cardDiv.className = 'card bg-dark text-white text-center wobble-horizontal';
        var record = db().filter({ regression_job: regressionTypes[i] }).first();
        var icon = document.createElement('i');
        if (record.regression_job.toUpperCase().includes('AWS')) {
            icon.className = "fab fa-10x fa-aws text-center mt-4";
            icon.style = 'color: #f7b71d';
        }

        else {
            icon.className = "fab fa-10x fa-windows text-center mt-4";
            icon.style = 'color: #1b7fbd';
        }

        icon.class = "card-img-top";

        var childDiv = document.createElement('DIV');
        childDiv.className = 'card-body overviewChildDiv';
        var jobName = document.createElement('H5');
        jobName.className = 'card-title mb-4';
        var hr = document.createElement('HR');
        hr.style = 'border: 1px solid #eeeeee;';
        var buildID = document.createElement('H6');
        buildID.className = 'card-subtitle mb-3';
        var date = document.createElement('H6');
        date.className = 'card-subtitle mb-3 ';
        var totalTests = document.createElement('H6');
        totalTests.className = 'card-subtitle mb-3 ';
        var failed = document.createElement('H6');
        failed.className = 'card-subtitle mb-3 ';
        var regression = document.createElement('H6');
        regression.className = 'card-subtitle mb-3';
        var successPercent = document.createElement('H6');
        successPercent.className = 'card-subtitle';
        var jobNameText = document.createTextNode(record.regression_job.toString().replace(/_/g, ' '));
        var buildIDText = document.createTextNode("BUILD ID : " + record.jenkins_build_id);
        var dateText = document.createTextNode("DATE : " + record.regression_date);
        var totalTestsText = document.createTextNode("TOTAL TESTS : " + record.total_tests);
        var failedText = document.createTextNode("FAILED : " + record.failed);

        var spanRegression = document.createElement('SPAN');
        var regressionText = document.createTextNode("REGRESSIONS : " + record.regressions);

        spanRegression.className = 'badge badge-pill overview-badge-info mr-2';
        spanRegression.appendChild(regressionText);



        var spanPP = document.createElement('SPAN');
        var percent = record.pass_percentage.toString().split('%')[0];
        var successPercentText = document.createTextNode("PASS PERCENTAGE : " + record.pass_percentage);
        if (percent > 90)
            spanPP.className = 'badge badge-pill overview-badge-success mr-2';
        else if (percent > 80)
            spanPP.className = 'badge badge-pill overview-badge-warning mr-2';
        else
            spanPP.className = 'badge badge-pill overview-badge-danger mr-2';
        spanPP.appendChild(successPercentText);

        jobName.appendChild(jobNameText);
        buildID.appendChild(buildIDText);
        date.appendChild(dateText);
        totalTests.appendChild(totalTestsText);
        failed.appendChild(failedText);
        regression.appendChild(spanRegression);
        successPercent.appendChild(spanPP);
        childDiv.appendChild(jobName);
        childDiv.appendChild(hr);
        childDiv.appendChild(buildID);
        childDiv.appendChild(date);
        childDiv.appendChild(totalTests);
        childDiv.appendChild(failed);
        childDiv.appendChild(regression);
        childDiv.appendChild(successPercent);
        cardDiv.appendChild(icon);
        cardDiv.appendChild(childDiv);
        cardDiv.id = record.regression_job + "-" + record.jenkins_build_id;
        cardDiv.onclick = function () { openSummaryPage(this.id) };
        overviewGroup.appendChild(cardDiv);
    }
}

function buildRegressionListTable(reglist) {

    for (i = 0; i < reglist.length; i++) {
        db.insert(reglist[i]);
        var tr = document.createElement('TR');
        for (j = 0; j < 7; j++) {
            var td = document.createElement('TD')
            switch (j) {
                // case 0: var slno = document.createTextNode(i + 1);
                //     td.appendChild(slno);
                //     tr.appendChild(td);
                //     break;
                case 0:
                    var a = document.createElement('a');
                    var linkText = document.createTextNode(reglist[i].regression_job.toString().replace(/_/g, ' '));
                    a.appendChild(linkText);
                    a.className = "indexRegressionName";
                    a.target = "_blank";
                    a.title = reglist[i].regression_job;
                    a.href = 'summary.html?buildName=' + reglist[i].regression_job + "&buildId=" + reglist[i].jenkins_build_id;
                    td.appendChild(a);
                    tr.appendChild(td);
                    break;
                case 1: var jenkinsBuildId = document.createTextNode(reglist[i].jenkins_build_id);
                    var a = document.createElement('a');
                    var jenkinsBuildId = document.createTextNode(reglist[i].jenkins_build_id);
                    a.appendChild(jenkinsBuildId);
                    a.className = "indexRegressionName";
                    a.target = "_blank";
                    a.title = reglist[i].regression_job;
                    a.href = JENKINS_BASE_URL + reglist[i].regression_job + "/" + reglist[i].jenkins_build_id + "/";
                    td.appendChild(a);
                    tr.appendChild(td);
                    break;
                case 2: var total_tests = document.createTextNode(reglist[i].total_tests);
                    td.appendChild(total_tests);
                    tr.appendChild(td);
                    break;
                case 3: var failed = document.createTextNode(reglist[i].failed);
                    td.appendChild(failed);
                    tr.appendChild(td);
                    break;
                case 4: var regressions = document.createTextNode(reglist[i].regressions);
                    td.appendChild(regressions);
                    tr.appendChild(td);
                    break;
                case 5: var icon = document.createElement('i');
                    var text = document.createTextNode(reglist[i].pass_percentage);
                    var percent = reglist[i].pass_percentage.toString().split('%')[0];

                    if (percent > 90) {
                        icon.className = "fas fa-grin-stars fa-lg ml-4";
                        icon.style = "color: #009975";
                    } else if (percent > 80) {
                        icon.className = "fas fa-flushed fa-lg ml-4";
                        icon.style = "color: #ffbd39";
                    } else {
                        icon.className = "fas fa-sad-tear fa-lg ml-4";
                        icon.style = "color: #ca3e47";
                    }
                    td.appendChild(text);
                    td.appendChild(icon);
                    tr.appendChild(td);
                    break;
                case 6: var runDate = document.createTextNode(reglist[i].regression_date);
                    td.appendChild(runDate);
                    tr.appendChild(td);
                    break;
            }
        }
        $("#regressionTblBody").append(tr);
    }
    table = $('#regressiontbl').DataTable({
        "info": false
    }

    );
}

function formatDate(dateTime) {
    return moment.utc(dateTime).format('LL');
}

function openSummaryPage(paramsSet) {
    var buildName = paramsSet.toString().split("-")[0];
    var buildId = paramsSet.toString().split("-")[1];
    window.open('summary.html?buildName=' + buildName + "&buildId=" + buildId);
}

function updateWidgetsStats() {
    document.getElementById("totalReports").innerHTML = db().count();
    document.getElementById("totalTests").innerHTML = db().sum("total_tests");
    var latestReportID = db().first().jenkins_build_id;
    var latestReportName = db().first().regression_job;
    var dateOfCommencement = moment(db().last().run_date);
    var todaysDate = moment().format("DD MMM YYYY");
    document.getElementById("mostRecent").innerHTML = "<a href=summary.html?buildName=" + latestReportName + "&buildId=" + latestReportID + "> Latest </a>";
    document.getElementById("dateCommence").innerHTML = moment(todaysDate).diff(dateOfCommencement, 'days') + " days";
}