var labelSet = [];
var dataSet = [];
var buildName;
var buildId;
var vertical;
var buildDate;
var table;
var filter;
var showStackTraceFlag = true;
var db = TAFFY();

$(document).ready(function () {

    buildName = extractParamsfromURL('buildName', 'Empty');
    buildId = extractParamsfromURL('buildId', 'Empty');
    vertical = extractParamsfromURL('vertical', 'Empty');
    buildDate = extractParamsfromURL('buildDate', 'Empty');
    filter = extractParamsfromURL('filter', 'Empty');

    if (buildName != 'Empty' && buildId != 'Empty' && vertical != 'Empty' && buildDate != 'Empty') {
        setTimeout(function () {
            $('body').addClass('loaded');
        }, 1000);
        $('#versionTag').text(VERSION);
        getTrends();
        setHeaders();
        displayLineChart();
        getTestsForVertical();
        resetAnalysisModalListener();
        $("#bug-text").keyup(function () {
            if (this.value) {
                $("#btnAddAnalysisGo").attr("disabled", false);
            } else {
                $("#btnAddAnalysisGo").attr("disabled", true);
            }
        });

        $("#comment-text").keyup(function () {
            if (this.value) {
                $("#btnAddAnalysisGo").attr("disabled", false);
            } else {
                $("#btnAddAnalysisGo").attr("disabled", true);
            }
        });
        if (filter != 'Empty')
            applyFilterFromUrl(filter);

    } else {
        iziToast.error({
            title: 'Error',
            timeout: 2000,
            id: 'urlError',
            message: 'Sorry! The URL is invalid. Redirecting You!',
            position: 'topCenter',
            onClosing: function (instance, toast, closedBy) {
                window.location = 'index.html';
            }
        });

    }

});

function resetAnalysisModalListener() {
    $("#addAnalysis").on("hidden.bs.modal", function () {
        $('#errorDetail').attr("hidden", true);
        $('#stackTraceDetail').attr("hidden", true);
        $('#lblErrorDetail').attr("hidden", true);
        $('#lblStackTrace').attr("hidden", true);
        $('#btnShowStackTraceCommon').attr("hidden", false);
    });

    $("#addAnalysis").on('show.bs.modal', function () {
        if (table.rows({ selected: true }).count() === 1) {
            $('#bugHistory').val("");
            $('#commentHistory').text("");
            $('#bug-text').val("");
            $('#comment-text').text("");
            document.getElementById('btnCopyComment').hidden = false;
            document.getElementById('btnCopyBug').hidden = false;
            var id = table.row({ selected: true }).id().toString().split("_")[1];
            var data = table.rows(id).data()[0];
            console.log('rowData:' + data);
            var parentJob = buildName.toString().replace(/ /g, '_');
            var jobName = $.parseHTML(table.rows(id).data()[0][1])[0].title.trim();
            console.log('bugId:' + data[6]);
            console.log('comment:' + data[7]);
            if (data[6] != "N/A") {
                var bugId = $.parseHTML(data[6])[0].title.trim();
                console.log('bugId:' + bugId);
                $('#bug-text').val(bugId);
                $('#btnAddAnalysisGo').attr("disabled", false);
            }
            if (data[7] != "N/A") {
                var comment = $.parseHTML(data[7])[0].id.trim().split("_")[1];
                selectRadioByValue($.parseHTML(data[7])[0].id.trim().split("_")[0])
                console.log('comment:' + comment);
                $('#comment-text').text(comment);
                $('#btnAddAnalysisGo').attr("disabled", false);
            }

            var testName = table.rows(id).data()[0][2];
            var testsParam = table.rows(id).data()[0][4] == 'N/A' ? null : table.rows(id).data()[0][4];
            getHistory(parentJob, jobName, testName, testsParam);


        }
    });

}

function selectRadioByValue(value) {

    var radios = document.getElementsByName("issueTypeRadioOptions");

    for (var i = 0, len = radios.length; i < len; i++) {
        var r = radios[i]; // current radio button

        if (r.value === value) {
            r.checked = true; // set checked
        }

    }
}

function getHistory(parentJob, jobName, testName, testsParam) {
    db = null;
    db = TAFFY();
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/showHistory?parentJob=" + parentJob + "&jobName=" + jobName + "&testName=" + testName + "&testsParam=" + encodeURIComponent(testsParam),
        type: 'GET',
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            initialiseTaffy(res.test_trends);
            var prevBug = db({ bug: { isNull: false } }).first().bug ? db({ bug: { isNull: false } }).first().bug : "No data found in the system";
            var prevComment = db({ comment: { "!is": "" } }).first().comment ? db({ comment: { "!is": "" } }).first().comment : "No data found in the system";
            console.log(prevBug + "----" + prevComment);
            document.getElementById('bugHistory').value = prevBug;
            document.getElementById('commentHistory').innerHTML = prevComment;
            if (prevComment === 'No data found in the system')
                document.getElementById('btnCopyComment').hidden = true;
            if (prevBug === 'No data found in the system')
                document.getElementById('btnCopyBug').hidden = true;

        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function copyComment() {
    if ($('#commentHistory').text() != 'No data found in the system')
        document.getElementById('comment-text').innerHTML = $('#commentHistory').text();
    else {
        iziToast.error({
            title: 'Error',
            timeout: 2000,
            id: 'commentError',
            message: 'Sorry! Not a valid comment to copy',
            position: 'topCenter',
        });
    }
}

function copyBugID() {
    if ($('#bugHistory').val() != 'No data found in the system')
        document.getElementById('bug-text').value = $('#bugHistory').val();
    else {
        iziToast.error({
            title: 'Error',
            timeout: 2000,
            id: 'commentError',
            message: 'Sorry! Not a valid JIRA bug to copy',
            position: 'topCenter',
        });
    }
}

function initialiseTaffy(test_trends) {
    for (i = 0; i < test_trends.length; i++) {
        db.insert(test_trends[i]);
    }
}

function setHeaders() {
    $('#regressionName').text(buildName.toString().replace(/_/g, ' '));
    $('#verticalName').text('Vertical : ' + vertical);
    $('#regressionDate').text('Date : ' + decodeURIComponent(buildDate));

}

function extractParamsfromURL(parameter, defaultvalue) {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getTrends() {
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/getVerticalTrends?buildName=" + buildName + "&vertical=" + vertical,
        type: 'GET',
        async: false,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            initializeDataSets(res.trend_details);
        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function initializeDataSets(details) {
    for (i = 0; i < details.length; i++) {
        if (i == 9)
            break;
        labelSet.push(formatDate(details[i].date));
        dataSet.push(details[i].pass_percentage.toString().replace('%', ''));
    }
}


function displayLineChart() {
    var ctx = document.getElementById('line-chart').getContext("2d");

    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#28CFCD');
    gradientStroke.addColorStop(1, '#1A2880');

    var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
    gradientFill.addColorStop(0, "rgba(36, 205, 203, 0.6)");
    gradientFill.addColorStop(1, "rgba(26, 40, 128, 0.6)");

    new Chart(ctx, {
        type: 'line',
        data: {
            labels: labelSet,
            datasets: [{
                // label: "PASS PERCENTAGE",
                borderColor: gradientStroke,
                pointBorderColor: gradientStroke,
                pointBackgroundColor: gradientStroke,
                pointHoverBackgroundColor: gradientStroke,
                pointHoverBorderColor: gradientStroke,
                pointBorderWidth: 10,
                pointHoverRadius: 10,
                pointHoverBorderWidth: 1,
                pointRadius: 3,
                fill: true,
                backgroundColor: gradientFill,
                borderWidth: 4,
                data: dataSet
            }]
        },
        options: {
            legend: {
                display: false,
                position: "bottom"
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    title: function (tooltipItem, data) {
                        return "Pass Percentage";
                    },
                    label: function (tooltipItem, data) {
                        return tooltipItem.yLabel + " %";
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 20,
                    },
                    gridLines: {
                        drawTicks: false,
                        display: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'PERCENTAGE'
                    }

                }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent"
                    },
                    ticks: {
                        padding: 20,
                        fontColor: "rgba(0,0,0,0.5)",
                        fontStyle: "bold"
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'DATE'
                    }
                }]
            },
            animation: {
                duration: 4000,
            }
        }
    });
}

function formatDate(dateTime) {
    return moment.utc(dateTime).format('LL');
}

function applyFilterFromUrl(filter) {
    $('#filterSelect').val(filter);
    applyRemoveFilter();
}

function applyRemoveFilter() {
    var filterValue = $("#filterSelect option:selected").text();
    if (filterValue != 'All') {
        table.columns(5).search(filterValue).draw();
        if (filterValue == 'Failed') {
            table.columns(5).search('Failed|Regression', true, false).draw();
        }
        else if (filterValue == 'Passed') {
            table.columns(5).search('Passed|Fixed', true, false).draw();
        }
        else
            table.columns(5).search(filterValue, true, false).draw();

    } else {
        table.columns(5).search('').draw();
    }
}

function prepareDataTable() {

    table = $('#testExecutionResultTbl').DataTable({

        select: true
    });

    table.on('user-select', function (e, dt, type, cell, originalEvent) {
        if ($(originalEvent.target).index() === 0) {
            e.preventDefault();
        }
        if (originalEvent.target.nodeName === 'img') {
            e.preventDefault();
        }
    });

    $('#btnSelectAll').on('click', function () {
        table.rows({ search: 'applied' }).select();
    });
    $('#btnSelectNone').on('click', function () {
        table.rows({ search: 'applied' }).deselect();
    });

    table.on('select', function (e, dt, type, indexes) {
        selectCount = table.rows({ selected: true }).count();
        if (selectCount > 0) {
            $("#btnAddAnalysis").attr("hidden", false);
            if (selectCount === 1) {
                $("#btnShowStackTraceCommon").attr("hidden", false);
                $("#multistacktraceWarning").attr("hidden", true);
                $("#lblBugIDHistory").attr("hidden", false);
                $("#bugHistory").attr("hidden", false);
                $("#lblCommentHistory").attr("hidden", false);
                $("#commentHistory").attr("hidden", false);
                $("#multiHistoryWarning").attr("hidden", true);
            }

            else {
                $("#btnShowStackTraceCommon").attr("hidden", true);
                $("#multistacktraceWarning").attr("hidden", false);
                $("#lblBugIDHistory").attr("hidden", true);
                $("#bugHistory").attr("hidden", true);
                $("#lblCommentHistory").attr("hidden", true);
                $("#commentHistory").attr("hidden", true);
                $("#multiHistoryWarning").attr("hidden", false);
            }

        } else {
            $("#btnAddAnalysis").attr("hidden", true);
            $("#btnShowStackTraceCommon").attr("hidden", true);
            $("#multistacktraceWarning").attr("hidden", true);
        }
    });

    table.on('deselect', function (e, dt, type, indexes) {
        selectCount = table.rows({ selected: true }).count();
        if (selectCount > 0) {
            $("#btnAddAnalysis").attr("hidden", false);
            $("#btnAddComment").attr("hidden", false);
            if (selectCount === 1) {
                $("#btnShowStackTraceCommon").attr("hidden", false);
                $("#lblBugIDHistory").attr("hidden", false);
                $("#bugHistory").attr("hidden", false);
                $("#lblCommentHistory").attr("hidden", false);
                $("#commentHistory").attr("hidden", false);
                $("#multistacktraceWarning").attr("hidden", true);
                $("#multiHistoryWarning").attr("hidden", true);
            }
            else {
                $("#btnShowStackTraceCommon").attr("hidden", true);
                $("#multistacktraceWarning").attr("hidden", false);
                $("#lblBugIDHistory").attr("hidden", true);
                $("#bugHistory").attr("hidden", true);
                $("#lblCommentHistory").attr("hidden", true);
                $("#commentHistory").attr("hidden", true);
                $("#multiHistoryWarning").attr("hidden", false);
            }
        } else {
            $("#btnAddAnalysis").attr("hidden", true);
            $("#btnShowStackTraceCommon").attr("hidden", true);
            $("#multistacktraceWarning").attr("hidden", true);
        }
    });

}


function getTestsForVertical() {
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/verticaleachtest?buildName=" + buildName + "&buildId=" + buildId + "&vertical=" + vertical,
        type: 'GET',
        async: false,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            buildTestsTable(res);
        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function showStackTrace() {

    $('#errorDetail').attr("hidden", false);
    $('#stackTraceDetail').attr("hidden", false);
    $('#lblErrorDetail').attr("hidden", false);
    $('#lblStackTrace').attr("hidden", false);
    $('#btnShowStackTraceCommon').attr("hidden", true);

    $('#errorDetail').text("");
    $('#stackTraceDetail').text("");
    var selectedTestID = table.row({ selected: true }).id();
    $('#stacktraceModal').modal('toggle');
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/getStackTrace/" + selectedTestID,
        type: 'GET',
        async: true,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            if (res.errorStackTrace != null) {
                document.getElementById('errorDetail').innerHTML = res.errorDetails;
                document.getElementById('stackTraceDetail').innerHTML = res.errorStackTrace;
            }
            else
                $('#stackTraceDetail').text("No Stack Trace found for the test");
        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function buildTestsTable(details) {
    for (i = 0; i < details.length; i++) {
        var tr = document.createElement('TR');
        tr.setAttribute('id', details[i].id + "_" + i);
        for (j = 0; j < 9; j++) {
            var td = document.createElement('TD');
            switch (j) {
                case 0: var slno = document.createTextNode(i + 1);
                    td.appendChild(slno);
                    tr.appendChild(td);
                    break;
                case 1: var a = document.createElement('a');
                    var linkText = document.createTextNode(details[i].job_name.toString().split(' [')[0].replace(/_/g, ' '));
                    a.appendChild(linkText);
                    a.className = "indexRegressionName";
                    a.target = "_blank";
                    a.title = details[i].job_name;
                    a.href = JENKINS_BASE_URL + details[i].job_name.toString().split(' [')[0].trim() + "/" + details[i].build_id + "/";
                    td.appendChild(a);
                    tr.appendChild(td);
                    break;
                case 2: var testName = document.createTextNode(details[i].name);
                    td.appendChild(testName);
                    tr.appendChild(td);
                    break;
                case 3: var class_name = details[i].class_name;
                    if (class_name) {
                        var text = class_name;//.substring(0, class_name.lastIndexOf(".")) + "-" + class_name.substring(class_name.lastIndexOf(".") + 1);
                        var className = document.createTextNode(text);
                        className.id = class_name;
                        td.appendChild(className);
                    } else
                        td.appendChild(document.createTextNode('N/A'));
                    tr.appendChild(td);
                    break;
                case 4:
                    if ((details[i].param == null) || (details[i].param == "")) {
                        td.appendChild(document.createTextNode('N/A'));

                    } else {
                        var params = document.createTextNode(details[i].param);
                        td.appendChild(params);
                    }

                    tr.appendChild(td);
                    break;
                case 5: var status = document.createTextNode(details[i].status);
                    td.appendChild(status);
                    tr.appendChild(td);
                    break;
                case 6:
                    if (details[i].Bug_id != null) {

                        icon = document.createElement('i');
                        icon.className = "fas fa-external-link-alt ml-2";

                        var a = document.createElement('a');
                        var linkText = document.createTextNode(details[i].Bug_id);
                        a.appendChild(linkText);
                        a.appendChild(icon);
                        a.className = "badge badge-pill badge-bug-info mr-2 bugHover";
                        a.target = "_blank";
                        a.title = details[i].Bug_id;
                        a.href = JIRA_BASE_URL + details[i].Bug_id;

                        td.appendChild(a);
                    } else {
                        td.appendChild(document.createTextNode('N/A'));
                    }
                    tr.appendChild(td);
                    break;
                case 7:
                    if (details[i].comment != "") {
                        if (details[i].bug_reason != null) {
                            var span = document.createElement('SPAN');
                            var text = document.createTextNode(details[i].bug_reason);
                            span.className = 'badge badge-pill badge-issue-info mr-2';
                            span.id = details[i].bug_reason + "_" + details[i].comment;
                            span.appendChild(text);
                            td.className = 'wrapTd';
                            td.appendChild(span);
                        }
                        var comment = document.createTextNode(details[i].comment);


                        td.appendChild(comment);
                    } else {
                        td.appendChild(document.createTextNode('N/A'));
                    }
                    tr.appendChild(td);
                    break;

                case 8: icon = document.createElement('i');
                    icon.className = "far fa-eye mr-2";
                    var btn = document.createElement('BUTTON');
                    var text = document.createTextNode("History");
                    btn.className = "btn btn-success btn-show-history btn-sm";
                    btn.id = i;
                    btn.title = "Shows analysis history for this particular test";
                    btn.onclick = function () { showHistory(this.id) };
                    btn.appendChild(icon);
                    btn.appendChild(text);
                    td.className = 'options';
                    td.appendChild(btn);
                    tr.appendChild(td);
                    break;
            }
        }
        $("#testExecutionResultTbl tbody").append(tr);
    }
    prepareDataTable();
}

function showHistory(id) {
    console.log("myID" + id);
    var parentJob = buildName.toString().replace(/ /g, '_');
    var jobName = $.parseHTML(table.rows(id).data()[0][1])[0].title.trim();
    var testName = table.rows(id).data()[0][2];
    var testsParam = table.rows(id).data()[0][4] == 'N/A' ? null : table.rows(id).data()[0][4];
    console.log(table.rows(id).data());
    console.log(jobName);
    window.open('showHistory.html?parentJob=' + encodeURIComponent(parentJob) + "&jobName=" + encodeURIComponent(jobName) + '&testName=' + encodeURIComponent(testName) + "&testsParam=" + encodeURIComponent(testsParam), '_blank');
}

function reportAnalysisForTests() {
    var testIds = [];
    var selectedrows = table.rows({ selected: true }).ids();
    for (i = 0; i < selectedrows.count(); i++)
        testIds.push(selectedrows[i].toString().split("_")[0]);
    var jira = $('#bug-text').val().toString().trim() != "" ? $('#bug-text').val().toString().trim() : null;
    var comment = $('#comment-text').val().toString().trim() != "" ? $('#comment-text').val().toString().trim() : null;
    var issueType = $("input[name='issueTypeRadioOptions']:checked").val();
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/eachtest",
        dataType: 'json',
        type: 'PUT',
        async: false,
        contentType: 'application/json',
        data: JSON.stringify({ "bugId": jira, "comment": comment, "issue": issueType, "testId": testIds }),
        processData: false,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            $('#addBugModal').modal('toggle');
            iziToast.success({
                title: 'Success',
                timeout: 1500,
                message: 'Your analysis has successfully been reported for the selected test(s)',
                position: 'topCenter',
                onClosing: function (instance, toast, closedBy) {
                    $(".loading").hide();
                    location.reload();
                }
            });
        },
        error: function (res) {
            console.log(res);
            $('#addBugModal').modal('toggle');
            $(".loading").hide();
            iziToast.error({
                title: 'Error',
                message: 'Sorry! Request failed! Reason : ' + res.responseJSON.reason,
                id: 'ajaxError',
                position: 'topCenter',
                onOpened: function () {
                    var toast = document.getElementById('ajaxError');
                    iziToast.progress({}, toast).pause();
                }
            });
        },

        complete: function () {

        }
    });
}




