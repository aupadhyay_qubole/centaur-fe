var parentJob;
var jobName;
var testName;
var jobName;
var pOccurences = 0;
var fOccurences = 0;
var sOccurences = 0;
var rOccurences = 0;
var fiOccurences = 0;

$(document).ready(function () {
    testName = decodeURIComponent(extractParamsfromURL('testName', 'Empty'));
    testsParam = decodeURIComponent(extractParamsfromURL('testsParam', 'Empty'));
    parentJob = decodeURIComponent(extractParamsfromURL('parentJob', 'Empty'));
    jobName = decodeURIComponent(extractParamsfromURL('jobName', 'Empty'));
    setTimeout(function () {
        $('body').addClass('loaded');
    }, 1000);
    $('#versionTag').text(VERSION);
    setHeaders();
    getHistory();
    buildPolarChart();
});

function extractParamsfromURL(parameter, defaultvalue) {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function setHeaders() {
    $('#parentJob').text(parentJob.toString().replace(/_/g, ' '));
    $('#jobName').text(jobName.toString().replace(/_/g, ' '));
    $('#testName').text(testName);
}

function getHistory() {
    $.ajax({
        url: CENTAUR_API_BASE_URL + "/api/v1/showHistory?parentJob=" + parentJob + "&jobName=" + jobName + "&testName=" + testName + "&testsParam=" + encodeURIComponent(testsParam),
        type: 'GET',
        async: false,
        beforeSend: function () {
            $(".loading").show();
        },
        success: function (res) {
            console.log(res);
            buildHistoryTable(res.test_trends);
        },
        complete: function () {
            $(".loading").hide();
        }
    });
}

function buildPolarChart() {
    new Chart(document.getElementById("polar-chart"), {
        type: 'polarArea',
        data: {
            labels: ["Passed", "Failed", "Skipped", "Regression", "Fixed"],
            datasets: [
                {
                    label: "No of occurences",
                    backgroundColor: ["#05a19c", "#df7599", "#ffc785", "#e8c3b9", "#c45850"],
                    data: [pOccurences, fOccurences, sOccurences, rOccurences, fiOccurences]
                }
            ]
        },
        options: {
            animation: {
                duration: 3000,
            }
        }
    });
}

function buildHistoryTable(test_trends) {

    for (i = 0; i < test_trends.length; i++) {
        var tr = document.createElement('TR');
        for (j = 0; j < 5; j++) {
            var td = document.createElement('TD')
            switch (j) {
                case 0: var date = document.createTextNode(test_trends[i].date);
                    td.appendChild(date);
                    tr.appendChild(td);
                    break;

                case 1: var status = document.createTextNode(test_trends[i].status);
                    td.appendChild(status);
                    tr.appendChild(td);
                    if (test_trends[i].status === 'PASSED')
                        pOccurences++;
                    else if (test_trends[i].status === 'FAILED')
                        fOccurences++;
                    else if (test_trends[i].status === 'SKIPPED')
                        sOccurences++;
                    else if (test_trends[i].status === 'REGRESSION')
                        rOccurences++;
                    else if (test_trends[i].status === 'FIXED')
                        fiOccurences++;
                    break;
                case 2:
                    if (test_trends[i].bug != null) {
                        icon = document.createElement('i');
                        icon.className = "fas fa-external-link-alt ml-2";

                        var a = document.createElement('a');
                        var linkText = document.createTextNode(test_trends[i].bug);
                        a.appendChild(linkText);
                        a.appendChild(icon);
                        a.className = "badge badge-pill badge-bug-info mr-2 bugHover";
                        a.target = "_blank";
                        a.title = test_trends[i].bug;
                        a.href = JIRA_BASE_URL + test_trends[i].bug;
                        td.appendChild(a);
                    } else {
                        td.appendChild(document.createTextNode("N/A"));
                    }
                    tr.appendChild(td);
                    break;
                case 3:
                    if (test_trends[i].issueType != null) {
                        var span = document.createElement('SPAN');
                        var text = document.createTextNode(test_trends[i].issueType);
                        span.className = 'badge badge-pill badge-issue-info mr-2';
                        span.style = 'font-size : 15px;'
                        span.appendChild(text);
                        td.appendChild(span);
                    } else {
                        td.appendChild(document.createTextNode("N/A"));
                    }
                    tr.appendChild(td);
                    break;
                case 4:
                    if (test_trends[i].comment != "" || test_trends[i].comment != null) {
                        td.className = 'wrapTdBig';
                        td.appendChild(document.createTextNode(test_trends[i].comment));
                    } else {
                        td.appendChild(document.createTextNode("N/A"));
                    }
                    tr.appendChild(td);
                    break;
            }
        }
        $("#showHistoryTbl").append(tr);
    }
    $('#showHistoryTbl').DataTable({
        'sorting': false
    });
}

function formatDate(dateTime) {
    return moment.utc(dateTime).format('LL');
}

function openSummaryPage(paramsSet) {
    var buildName = paramsSet.toString().split('-')[0];
    var buildId = paramsSet.toString().split('-')[1];

    window.open('summary.html?buildName=' + buildName + "&buildId=" + buildId, '_blank');
}