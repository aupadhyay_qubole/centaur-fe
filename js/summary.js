var passedColor = "#41aaa8";
var failedColor = "#cd4545";
var SkippedColor = "#fab95b";
var verticalDataSet = [];
var passedDataSet = [];
var failedDataSet = [];
var skippedDataSet = [];
var totalDataSet = [];
var totalpassed = 0;
var totalfailed = 0;
var totalskipped = 0;
var buildName;
var buildId;
var buildDate;

$(document).ready(function () {

  buildName = extractParamsfromURL('buildName', 'Empty');
  buildId = extractParamsfromURL('buildId', 'Empty');
  if (buildName != 'Empty' && buildId != 'Empty') {
    WebFont.load({
      google: {
        families: ['Ubuntu']
      }
    });
    $('#versionTag').text(VERSION);
    setTimeout(function () {
      $('body').addClass('loaded');
    }, 1000);
    getVerticalDetails();
    buildPieChart();
    buildBarChart();
  } else {
    iziToast.error({
      title: 'Error',
      timeout: 2000,
      message: 'Sorry! The URL is inavlid. Redirecting You!',
      position: 'topCenter',
      onClosing: function (instance, toast, closedBy) {
        window.location = 'index.html';
      }
    });
  }


});

function extractParamsfromURL(parameter, defaultvalue) {
  var urlparameter = defaultvalue;
  if (window.location.href.indexOf(parameter) > -1) {
    urlparameter = getUrlVars()[parameter];
  }
  return urlparameter;
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = value;
  });
  return vars;
}

function buildPieChart() {
  var piechart = document.getElementById("pie-chart").getContext('2d');
  new Chart(piechart, {
    type: 'outlabeledPie',
    percentPrecision: 2,
    data: {
      labels: ["PASS", "FAIL", "SKIPPED"],
      datasets: [{
        backgroundColor: [passedColor, failedColor, SkippedColor],
        data: [totalpassed, totalfailed, totalskipped],
        borderColor: [
          "#ffffff",
          "#ffffff",
          "#ffffff"
        ],
        borderWidth: [1, 1, 1]
      }]
    },
    precision: 2,
    options: {
      zoomOutPercentage: 30,
      responsive: true,
      animation: {
        duration: 3000,
      },
      plugins: {
        legend: false,

        outlabels: {
          font: {
            family: 'Ubuntu',
            size: 16,
            minSize: 14,
            maxSize: 16
          },
          text: '%l - %v (%p.)',
        }
      }
    }
  });
}

function buildBarChart() {
  new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: verticalDataSet,
      datasets: [{
        label: 'Passed',
        data: passedDataSet,
        backgroundColor: passedColor,
      },
      {
        label: 'Failed',
        data: failedDataSet,
        backgroundColor: failedColor,
      },
      {
        label: 'Skipped',
        data: skippedDataSet,
        backgroundColor: SkippedColor,
      }]
    },
    options: {
      scales: {
        xAxes: [{
          stacked: true,
          gridLines: { display: false },
          scaleLabel: {
            display: true,
            labelString: 'DIFFERENT VERTICALS',
            fontColor: "#313131",
            fontSize: 15,
            fontFamily: 'Ubuntu',
            fontStyle: 'bold'
          }
        }],
        yAxes: [{
          stacked: true,
          gridLines: { display: true },
          scaleLabel: {
            display: true,
            labelString: 'NUMBER OF TEST CASES',
            fontColor: "#313131",
            fontSize: 15,
            fontFamily: 'Ubuntu',
            fontStyle: 'bold'
          }
        }]
      },
      animation: {
        duration: 3000,
      },
    }
  });
}

function getVerticalDetails() {
  $.ajax({
    url: CENTAUR_API_BASE_URL + "/api/v1/regresult?buildName=" + buildName + "&buildId=" + buildId,
    type: 'GET',
    async: false,
    beforeSend: function () {
      $(".loading").show();
    },
    success: function (res) {
      console.log(res);
      $('#regressionName').text(res.regression_job.toString().replace(/_/g, ' '));
      $('#regressionDate').text(formatDate(res.date));
      buildDate = formatDate(res.date);
      buildDetailsTable(res.details);
    },
    complete: function () {
      $(".loading").hide();
    }
  });
}

function formatDate(dateTime) {
  return moment.utc(dateTime).format('LL');
}

function buildDetailsTable(details) {

  var analysisComplete = true;
  var table = document.getElementById('resultsTable');
  for (i = 0; i < details.length; i++) {
    var tr = document.createElement('TR');
    for (j = 0; j < 10; j++) {
      var td = document.createElement('TD')
      switch (j) {
        case 0: var a = document.createElement('a');
          var th = document.createElement('TH')
          var linkText = document.createTextNode(details[i].vertical);
          a.appendChild(linkText);
          a.className = "verticalName";
          a.target = "_blank";
          a.title = "Vertical-" + details[i].vertical;
          a.href = 'analysis.html?buildName=' + buildName + "&buildId=" + buildId + "&vertical=" + details[i].vertical + "&buildDate=" + buildDate;
          th.appendChild(a);
          tr.appendChild(th);
          verticalDataSet.push(details[i].vertical);
          break;
        case 1:
          var a = document.createElement('a');
          var linkText = document.createTextNode(details[i].passed);
          a.appendChild(linkText);
          a.className = "passedCount";
          a.target = "_blank";
          a.title = "Passed Count";
          if (details[i].passed > 0) {
            a.href = 'analysis.html?buildName=' + buildName + "&buildId=" + buildId + "&vertical=" + details[i].vertical + "&buildDate=" + buildDate + "&filter=Passed";
          }
          td.appendChild(a);
          tr.appendChild(td);
          totalpassed += details[i].passed;
          passedDataSet.push(details[i].passed);
          break;
        case 2: var a = document.createElement('a');
          var linkText = document.createTextNode(details[i].failed);
          a.appendChild(linkText);
          a.className = "failedCount";
          a.target = "_blank";
          a.title = "Failed Count";
          if (details[i].failed > 0) {
            a.href = 'analysis.html?buildName=' + buildName + "&buildId=" + buildId + "&vertical=" + details[i].vertical + "&buildDate=" + buildDate + "&filter=Failed";
          }
          td.appendChild(a);
          tr.appendChild(td);
          totalfailed += details[i].failed;
          failedDataSet.push(details[i].failed);
          break;
        case 3: var a = document.createElement('a');
          var linkText = document.createTextNode(details[i].skipped);
          a.appendChild(linkText);
          a.className = "skippedCount";
          a.target = "_blank";
          a.title = "Skipped Count ";
          if (details[i].skipped > 0) {
            a.href = 'analysis.html?buildName=' + buildName + "&buildId=" + buildId + "&vertical=" + details[i].vertical + "&buildDate=" + buildDate + "&filter=Skipped";
          }
          td.appendChild(a);
          tr.appendChild(td);
          totalskipped += details[i].skipped;
          skippedDataSet.push(details[i].skipped);
          break;
        case 4: var a = document.createElement('a');
          var linkText = document.createTextNode(details[i].total);
          a.appendChild(linkText);
          a.className = "totalCount";
          a.target = "_blank";
          a.title = "Total Count ";
          if (details[i].total > 0) {
            a.href = 'analysis.html?buildName=' + buildName + "&buildId=" + buildId + "&vertical=" + details[i].vertical + "&buildDate=" + buildDate;
          }
          td.appendChild(a);
          tr.appendChild(td);
          totalDataSet.push(details[i].total);
          break;
        case 5: td.appendChild(document.createTextNode(details[i].count_env_issues));
          tr.appendChild(td);
          break;
        case 6: td.appendChild(document.createTextNode(details[i].count_data_issues));
          tr.appendChild(td);
          break;
        case 7: td.appendChild(document.createTextNode(details[i].count_test_issues));
          tr.appendChild(td);
          break;
        case 8: td.appendChild(document.createTextNode(details[i].bugs_attached));
          tr.appendChild(td);
          break;
        case 9:
          var failureSum = details[i].failed;
          var analysisNotDone = details[i].analysis;

          icon = document.createElement('i');

          if (failureSum === 0) {
            icon.className = "fas fa-check mr-2";
            icon.style = "color: #05a19c";
            var span = document.createElement('SPAN');
            var text = document.createTextNode('Not Required');
            span.className = 'badge badge-pill badge-not-required mr-2';
            span.appendChild(text);
          }
          else if (analysisNotDone === 0) {
            icon.className = "fas fa-check mr-2";
            icon.style = "color: #05a19c";
            var span = document.createElement('SPAN');
            var text = document.createTextNode('Complete');
            span.className = 'badge badge-pill badge-complete mr-2';
            span.appendChild(text);
          }
          else if (analysisNotDone > 0 && failureSum != analysisNotDone) {
            icon.className = "fas fa-spinner mr-2";
            icon.style = "color: #112f91";
            var span = document.createElement('SPAN');
            var text = document.createTextNode('In Progress');
            span.className = 'badge badge-pill badge-in-progress mr-2';
            span.appendChild(text);
            analysisComplete = false;
          }
          else {
            icon.className = "fas fa-times mr-2";
            icon.style = "color: #cd4545";
            var span = document.createElement('SPAN');
            var text = document.createTextNode('Yet To Start');
            span.className = 'badge badge-pill badge-yet-to-start mr-2';
            span.appendChild(text);
            analysisComplete = false;
          }

          td.appendChild(icon);
          td.appendChild(span);
          tr.appendChild(td);
          break;
      }
    }
    $("#resultsTable tbody").append(tr);
  }
  $('#resultsTable').DataTable({
    "paging": false,
    "ordering": false,
    "info": false
  });
  if (analysisComplete)
    $('#analysisComplete').removeAttr('hidden');
  else
    $('#analysisIncomplete').removeAttr('hidden');
}

function toggleCommentModal(jobName) {
  $('#job-name').text(jobName)
  $("#jobCommentModal").modal('toggle'); //see here usage
}

function addVerticalComment() {
  var comment = $('#message-text').val();
  alert(comment);
}